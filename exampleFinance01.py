from pandas_datareader import data
import matplotlib.pyplot as plt
import pandas as pd

'''
   https://www.investopedia.com/articles/06/probabilitydistribution.asp

   https://www.learndatasci.com/tutorials/python-finance-part-2-intro-quantitative-trading-strategies/

   http://pbpython.com/amortization-model.html
   https://lectures.quantecon.org/py/pandas.html

'''
def random_walk_price(msft, end_date, Periods=30, iters=100):

  def random_norm_hist(hst, wide=.5):
      return np.random.normal((hst[1][e] + hst[1][e-1])/2, wide*(hst[1][e] - hst[1][e-1]))

  def random_uni_hist(hst):
      return np.random.rand()*(hst[1][e] - hst[1][e-1]) + hst[1][e-1] 

  base_close = msft.loc[end_date]

  d=msft.diff()#shift(1)

  msft['DIFF']=d #msft - d

  msft['DIFF'] = msft['DIFF'].fillna(0)

  import numpy as np

  hist = np.histogram (msft['DIFF'], bins=20)

  cdf=np.cumsum(hist[0])
  max_=float(cdf[cdf.shape[0] - 1])

  cumu = cdf/max_

  pr=np.zeros(Periods)

  for e in range(iters):
    np.random.seed(e)
    mc = [np.argmax(cumu >e)+1 for e in np.random.rand(Periods)]
    pr = pr + np.cumsum(np.asarray([random_uni_hist(hist) for e in mc]))

  pr = pr/iters+base_close

  return pr

def moving_average(msft, short=20, long_=100):

  # Calculate the 20 and 100 days moving averages of the closing prices
  short_rolling_msft = msft.rolling(window=short).mean()
  long_rolling_msft = msft.rolling(window=long_).mean()

  # Plot everything by leveraging the very powerful matplotlib package
  fig, ax = plt.subplots(figsize=(16,9))

  ax.plot(msft.index, msft, label='MSFT')
  ax.plot(short_rolling_msft.index, short_rolling_msft, label='20 days rolling')
  ax.plot(long_rolling_msft.index, long_rolling_msft, label='100 days rolling')

  ax.set_xlabel('Date')
  ax.set_ylabel('Adjusted closing price ($)')
  ax.legend()

  plt.show()


def getData_quotes(defines, file_name = "data_quotes.pkl" ,from_disk=False):

  if not from_disk:
    # We would like all available data from 01/01/2000 until 12/31/2016.
    start_date =  defines['dates']['start']
    end_date = defines['dates']['end']

    # User pandas_reader.data.DataReader to load the desired data. As simple as that.
    panel_data = data.DataReader(defines['tickers'], 'yahoo', start_date, end_date)

    panel_data.to_pickle(file_name)
  else:
    panel_data = pd.read_pickle(file_name)

  return panel_data


def main1():
  # Define the instruments to download. We would like to see Apple, Microsoft and the S&P500 index.
  defines = {'tickers':['AAPL', 'MSFT', '^GSPC'],\
             'dates':{'start': '2014-1-01', 'end': '2018-07-25'}}
  
  panel_data = getData_quotes(defines, from_disk=True)

  close = panel_data['Close']

  # Getting all weekdays between 01/01/2000 and 12/31/2016
  all_weekdays = pd.date_range(start=defines['dates']['start'], end=defines['dates']['end'], freq='B')

  # How do we align the existing prices in adj_close with our new set of dates?
  # All we need to do is reindex close using all_weekdays as the new index
  close = close.reindex(all_weekdays)

  # Reindexing will insert missing values (NaN) for the dates that were not present
  # in the original set. To cope with this, we can fill the missing by replacing them
  # with the latest available price for each instrument.
  close = close.fillna(method='ffill')

  close.to_pickle('close_data.pkl')

  msft = close.loc[:, 'MSFT']

  moving_average(msft)

  '''

  '''

  pr = random_walk (msft, defines['dates']['end'], Periods=30)

  plt.plot(pr)
  plt.show()

import numpy as np

def main2():
    data = pd.read_pickle('close_data.pkl')
    # Log returns - First the logarithm of the prices is taken and the the difference of consecutive (log) observations
    log_returns = np.log(data).diff()

    log_returns_cumsum = log_returns.cumsum()
    c_relative = 100*(np.exp(log_returns_cumsum) - 1)


    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(16,12))

    for c in log_returns:
        print c
        ax1.plot(log_returns.index, log_returns[c].cumsum(), label=str(c))

    ax1.set_ylabel('Cumulative log returns')
    ax1.legend(loc='best')

    for c in log_returns:
        ax2.plot(log_returns.index, 100*(np.exp(log_returns[c].cumsum()) - 1), label=str(c))

    ax2.set_ylabel('Total relative returns (%)')
    ax2.legend(loc='best')

    plt.show()

def main4():
   data = pd.read_pickle('data_quotes.pkl')

   dff = data['Volume']['AAPL'].diff(1)
   cls = data['Close']['AAPL']

   fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(16,12))
   ax1.plot(dff)
   ax2.plot(cls)
   plt.show()

def random_walk_pc(msft, end_date, Periods=30, iters=100):
    pass


def main3():
    defines1 = {'tickers':['^MERV'],\
             'dates':{'start': '2000-1-01', 'end': '2018-07-25'}}
  
    panel_data = getData_quotes(defines1, file_name = "merval_quotes.pkl", from_disk=True)

    print panel_data['Close'].tail()
    panel_data['log'] = np.log(panel_data['Close'])
    panel_data['log_returns'] = panel_data['log'].diff().fillna(0)
    panel_data['pct_change'] = panel_data['Close'].pct_change(1).fillna(0)
    print panel_data['pct_change'].tail()

    base_close = panel_data['pct_change'].loc[defines1['dates']['end']]
    
    hist = np.histogram (panel_data['pct_change'], bins=20)

    cdf=np.cumsum(hist[0])
    max_=float(cdf[cdf.shape[0] - 1])

    cumu = cdf/max_
    Periods = 250
    iters = 1000

    def random_uni_hist(hst):
        return np.random.rand()*(hst[1][e] - hst[1][e-1]) + hst[1][e-1] 

    pr=np.zeros(Periods)

    for e in range(iters):
        np.random.seed(np.random.randint(32))
        mc = [np.argmax(cumu >e)+1 for e in np.random.rand(Periods)]
        pr = pr + np.cumsum(np.asarray([random_uni_hist(hist) for e in mc]))
        #pr = pr + np.asarray([random_uni_hist(hist) for e in mc])
    pr = pr/iters

    plt.plot((pr))
    plt.show()

    dates_per = pd.date_range('2018-07-27', periods=Periods, freq='B')
         

if __name__ == "__main__":
    main3()
