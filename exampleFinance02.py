from pandas_datareader import data
import matplotlib.pyplot as plt
import pandas as pd

usdars = pd.read_csv('USD_ARS.csv', decimal=',')

merval = pd.read_pickle('merval_quotes.pkl')['Close']['^MERV']
usdars['Fecha'] = pd.to_datetime(usdars.Fecha)

usdars.columns=['Date', 'Close', 'Open', 'Max', 'Min', 'pcvar']

#usdars.set_index('Date', inplace=True)

mer=pd.DataFrame({'Date':merval.index, 'Close':merval.values})

m=pd.merge(mer, usdars, on='Date')

m['clus']=m['Close_x']/m['Close_y']
#print usdars.head()

print mer.head(), mer.columns

print m.head()

import numpy as np
plt.plot(m['Date'], np.log(m['clus']))
plt.grid()
plt.show()


